from __future__ import division, print_function
from functools import reduce
import moviepy.editor as mpy
from moviepy.video.fx.all import resize
#import vpython as vp
import operator as op
import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
np.set_printoptions(threshold=np.inf)
#vec = vp.vector
#http://www.faculty.umassd.edu/j.wang/book/prog/ch07/Program_7.2_laplace.py

def compose(f_list):
    #Composes a list of functions of one variable
    def _compose(f, g):
        return lambda x: f(g(x))
    if len(f_list)>1:
        return reduce(_compose,f_list)
    return f_list[0]

class BoundaryConditionFactory(object):
    def __init__(self, array_dimensions):
        self.functions=[]
        self.array_dimensions=array_dimensions

    @property
    def boundary_condition(self):
        return compose(self.functions)

    @staticmethod
    def _draw_shape(array,cond,value=1):
        #cond should be a scalar bool function of the indices
        grid=np.meshgrid(*map(np.arange,array.shape)) #Index array
        array[np.vectorize(cond)(*grid)]=value
        return array

    def add_shape(self,cond,value=1):
        self.functions.append(lambda arr: self._draw_shape(arr,cond,value))

    def add_box(self,position,size,value=1):
        #Position refers to top left etc. corner
        #Boundaries
        assert len(position)==len(size)==len(self.array_dimensions) #Ensure dimensionality is respected

        def cond(*coords):
            return all(position[i]<=coords[i]<=position[i]+size[i] for i in range(len(position)))

        def make_box(arr):
            return self._draw_shape(arr,cond,value)

        self.functions.append(make_box)

    def add_circle(self,position,radius,value=1):
        assert len(position)==len(self.array_dimensions) #Ensure dimensionality is respected

        def cond(*coords):
            return sum(map(lambda coord: pow(coord,2),map(op.sub,coords,position)))<=radius**2

        def make_circle(arr):
            return self._draw_shape(arr,cond,value)

        self.functions.append(make_circle)

    def add_astroid(self,position,a,value=1):
        #a is a radius like parameter
        assert len(position) == len(self.array_dimensions)  # Ensure dimensionality is respected

        def cond(*coords):
            x,y=coords
            return pow((x-position[0]),3)+pow(y-position[1],3)<=a**3
            #return sum(map(lambda coord: pow(coord, (2./3)), map(op.sub, coords, position))) <= a ** (2./3)

        def make_astroid(arr):
            return self._draw_shape(arr, cond, value)

        self.functions.append(make_astroid)

class ElectricFieldSimulation(object):

    def __init__(self,boundary_condition,array_dimensions):
        self.boundary_condition=boundary_condition
        self.array_dimensions=array_dimensions
        self.arr=boundary_condition(np.zeros(array_dimensions,dtype=np.float64))
        self.states=[]
        self._add_state()

    @property
    def data(self):
        return np.dstack(self.states)

    def _add_state(self):
        self.states.append(np.copy(self.arr))

    @staticmethod
    def relax(V):
        # each value is average of its 4 nearest neighbors
        # implements EQN 7.6
        V[1:-1, 1:-1] = (V[1:-1, :-2] + V[1:-1, 2:] + V[:-2, 1:-1] + V[2:, 1:-1]) / 4
        return V

    def step(self):
        self.arr=self.boundary_condition(self.relax(self.arr))
        self._add_state()

class Player(object):
    def __init__(self,data):
        self.data=self.normalize(np.copy(data))
        self.array_dimensions=(data.shape[0],data.shape[1])

    def colorize(self,frame):
        R=np.zeros(self.array_dimensions,dtype=np.uint8)
        G=np.zeros(self.array_dimensions,dtype=np.uint8)
        B=np.zeros(self.array_dimensions,dtype=np.uint8)

        R_areas=frame<0
        B_areas=np.logical_not(R_areas)
        R[R_areas]=256*np.abs(frame[R_areas])
        B[B_areas] = 256 * frame[B_areas]

        return np.dstack((R,G,B))

    def create_movie(self,fps=25,size=(1000,1000),name='movie'):
        Images=list(map(self.colorize,(self.data[:,:,i] for i in range(self.data.shape[-1]))))
        clip=mpy.ImageSequenceClip(Images,fps=fps)
        clip=resize(clip,newsize=size)
        clip.write_videofile(name+".mp4")

    def make_graphs(self):
        V=np.copy(self.data[:,:,-1])
        Ex, Ey = np.gradient(-V)
        V, Ex, Ey = np.transpose(V), np.transpose(Ex), np.transpose(Ey)
        X, Y = np.meshgrid(range(V.shape[0]), range(V.shape[1]))
        plt.figure()
        plt.contour(V,14)
        plt.quiver(X[::2, ], Y[::2, ], Ex[::2, ], Ey[::2, ],  # stride 2 in y dir
                   width=0.004, minshaft=1.5, minlength=0, scale=10.)
        plt.xlabel('x')
        plt.ylabel('y')

        plt.figure()  # Fig.2, surface plot
        ax = plt.subplot(111, projection='3d')
        ax.plot_surface(X, Y, V, rstride=1, cstride=1, cmap=plt.cm.jet, lw=0)
        ax.set_xlabel('x'), ax.set_ylabel('y'), ax.set_zlabel('V')
        plt.show()


    @staticmethod
    def normalize(data):
        return data/np.amax(data)

if __name__ == '__main__':
    """
    bcf=BoundaryConditionFactory((60,60))
    bcf.add_circle((30,30),10)
    efs=ElectricFieldSimulation(bcf.boundary_condition,(60,60))
    for _ in range(200):
        efs.step()
    p=Player(efs.data)
    p.create_movie(name='circle')

    """
    """
    bcf=BoundaryConditionFactory((60,60))
    bcf.add_circle((10,10),7,value=1)
    bcf.add_box((40,40),(10,15),value=-1)
    efs=ElectricFieldSimulation(bcf.boundary_condition,(60,60))
    for _ in range(200):
        efs.step()
    p=Player(efs.data)
    p.make_graphs()
    """

    bcf=BoundaryConditionFactory((60,60))
    bcf.add_circle((10,10),7)
    bcf.add_circle((50,50),7)
    bcf.add_circle((10,50),7,value=-1)
    bcf.add_circle((50,10),7,value=-1)
    efs=ElectricFieldSimulation(bcf.boundary_condition,(60,60))
    for _ in range(200):
        efs.step()
    p=Player(efs.data)
    p.make_graphs()


