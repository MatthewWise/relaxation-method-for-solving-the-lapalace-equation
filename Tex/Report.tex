\documentclass{article}

%\usepackage[version=3]{mhchem} % Package for chemical equation typesetting
\usepackage{siunitx} % Provides the \SI{}{} and \si{} command for typesetting SI units
	\sisetup{separate-uncertainty = true, per-mode = fraction}
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{subfig}
\usepackage{multirow}
\usepackage{hyperref}
\hypersetup{
	colorlinks   = true, %Colours links instead of ugly boxes
}

\usepackage{fancyvrb}
\usepackage{natbib} % Required to change bibliography style to APA
\usepackage{amsmath} % Required for some math elements
\usepackage[parfill]{parskip}

%This makes figures appear in the appropriate section/subsection. No idea how it works
\usepackage[section]{placeins}

\makeatletter
\AtBeginDocument{%
	\expandafter\renewcommand\expandafter\subsection\expandafter{%
		\expandafter\@fb@secFB\subsection
	}%
}
\makeatother


\setlength\parindent{0pt} % Removes all indentation from paragraphs

%\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

\title{Solving the Poisson Equation via the Self Consistent Method}
\author{Matthew Wise}

\date{\today}

\begin{document}

\maketitle

\begin{abstract}
	
This article explores the use of the relaxation method to numerically solve the two dimensional Laplace Equation with Dirichlet boundaries. This allows the discovery of the entire potential field produced by any arrangement of known potential surfaces.
	
\end{abstract}

\section{Introduction}

\subsection{Theory}

The differential form of Gauss's Law in two dimensions, called the Poisson Equation, is given by:

\begin{equation}
\label{Poisson}
\frac{\partial^2 V}{\partial x^2}+\frac{\partial^2 V}{\partial y^2}=-\frac{\sigma}{\epsilon_0}
\end{equation}

If charge surface density $\sigma=0$ everywhere within the region, Equation \ref{Poisson} reduces to the Laplace Equation:

\begin{equation}
\label{Laplace}
\frac{\partial^2 V}{\partial x^2}+\frac{\partial^2 V}{\partial y^2}=0
\end{equation}

While solutions to this equation are certainly not unique, for a given set of boundary conditions, and taking into account Gauge Freedom (that Maxwell's Laws are covariant under the $V^\prime=V+C$ transform) all solutions will be physically equivalent and represent the true topology of the potential field. However, Laplace's Equation is only analytically solvable for a small number of simple cases. For the rest, we turn to numeric work.

\subsection{Computational Approach}

The approach employed here is called the ``Self Consistent Method'', an approach that iteratively improves on an initial guess until convergence. The function applied iteratively is found by discretizing Equation \ref{Laplace} on an $i,j$ grid:

\begin{equation}
\label{discretized Laplace}
V_{i,j}=\frac 14 \left[V_{i-1,j}+V_{i+1,j}+V_{i,j-1}+V_{i,j+1} \right ]
\end{equation}

which is semantically equivalent to setting each to cell to average of neighbors. In psuedocode, the algorithm looks like:

\begin{Verbatim}[frame=leftline,numbers=left,tabsize=4]
Initialize array with a guess at the answer
Unitl convergence:
	Apply boundary conditions
	Set each cell to the average of the 4 surrounding cells
\end{Verbatim}

In python, the function to transform the array as described in Equation \ref{discretized Laplace} is as follows:

\begin{Verbatim}[frame=leftline,numbers=left,tabsize=4]
def relax(V):
    V[1:-1, 1:-1] = (V[1:-1, :-2] + V[1:-1, 2:] 
    + V[:-2, 1:-1] + V[2:, 1:-1]) / 4
    
    return V
\end{Verbatim}

The full code, including some movies of the relaxation process, is available at my BitBucket at:

\href{https://bitbucket.org/MatthewWise/relaxation-method-for-solving-the-lapalace-equation}{https://bitbucket.org/MatthewWise/relaxation-method-for-solving-the-lapalace-equation}

\section{Data}

While I can't reasonably embed videos into a report, I have some static representations of solutions to include. The method I used is very flexible, capable of easily representing any shape which can be mathematically described on the Cartesian grid. Here a couple of examples:

\begin{figure}[htbp]
	\begin{center}
		
		\includegraphics[width=.5\paperwidth,keepaspectratio]{../Example_1_Contour}
		
		\caption{
			A contour plot of the potential field surrounding a $+\SI{1}{\volt}$ circle centered at the origin.
		}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		
		\includegraphics[width=.5\paperwidth,keepaspectratio]{../Example_1_3D}
		
		\caption{
			A 3D plot of the potential field surrounding a $+\SI{1}{\volt}$ circle centered at the origin.
		}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		
		\includegraphics[width=.5\paperwidth,keepaspectratio]{../Example_2_Contour}
		
		\caption{
			A contour plot of the potential field surrounding a circle and square of $\SI{-1}{\volt}$ and $+\SI{1}{\volt}$, respectively.
		}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		
		\includegraphics[width=.5\paperwidth,keepaspectratio]{../Example_2_3D}
		
		\caption{
			A 3D plot of the potential field surrounding a circle and square of $\SI{-1}{\volt}$ and $+\SI{1}{\volt}$, respectively.
		}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		
		\includegraphics[width=.5\paperwidth,keepaspectratio]{../Example_3_Contour}
		
		\caption{
			A contour plot of the potential field surrounding a series of circles alternating between $+\SI{1}{\volt}$ and $\SI{-1}{\volt}$.
		}
	\end{center}
\end{figure}

\begin{figure}[htbp]
	\begin{center}
		
		\includegraphics[width=.5\paperwidth,keepaspectratio]{../Example_3_3D}
		
		\caption{
			A 3D plot of the potential field surrounding a series of circles alternating between $+\SI{1}{\volt}$ and $\SI{-1}{\volt}$.
		}
	\end{center}
\end{figure}

\section{Analysis/Conclusion}

While the Laplace Equation cannot be solved analytically for these complicated cases, these numerical results appear in line with expectations. Field lines, found by taking a numerical gradient, point in the directions one would suspect (away from positive potentials and towards negative potentials), and the 3D plots show the expected smooth transitions and saddle points. This likely means that the method works, which is good. While imprecise, numerical approaches like this are valuable for the range of situations they can successfully represent. While I chose these three cases as examples, much more complicated arrangements are possible, and as easy to test as they are to describe.

\end{document}